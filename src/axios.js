import axios from "axios"

const axiosInstance = axios.create({
    baseURL: "https://spring-state-marin-2.herokuapp.com",
    //baseURL: "http://localhost:8080/",
    headers: {
        post: {
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Headers":
                "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
        },

    }
});


export default axiosInstance;