import React from "react"
import {  Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";

class AddMeasureToSensor extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            date: "",
            time:"",
            value:""
           
        }
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let sensor = {
            sensorId: this.state.id,
            date: this.state.date,
            time: this.state.time,
            value: this.state.value
           
        }
        console.log(sensor);
        

        axiosInstance.put("/measure/update", sensor)
            .then(
                res => {
                    sensor = res.data;
                }
            )
            .catch(error => {
            });
    }


    render() {
        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {

            return (
                <React.Fragment>
                    <form onSubmit={this.handleSubmit}>
                       
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Date:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    name="date"
                                    placeholder="date"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type="date"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Time:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    name="time"
                                    placeholder="time"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type="time"
                                />
                            </div>


                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                   Value:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="value"
                                    name="value"
                                    placeholder="value"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>
                           
                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="primary"
                                id="create"
                                type="submit"
                            >
                                Adauga masuratoare
                            </Button>
                        </div>
                    </form>
                 

                </React.Fragment>



            )
        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }
}

export default AddMeasureToSensor;