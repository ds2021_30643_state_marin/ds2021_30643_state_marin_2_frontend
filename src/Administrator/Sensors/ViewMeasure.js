import React from "react"
import axiosInstance from "../../axios";
import {
    Table, TableCell, TableBody, TableContainer,
    TableHead, TableRow, Paper
} from '@mui/material';
import Container from "@material-ui/core/Container";

class ViewMeasure extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            measures: [],

        }

    }

    componentDidMount() {
        axiosInstance.get("/senzor/measure/" + this.state.id)
            .then(res => {
                const val = res.data;
                this.setState({
                    measures: val
                });
                console.log(val);
                console.log(this.state.measures);
            }
            )
            .catch(error => {
                console.log(error);
            })
    }

    add() {
        this.props.history.push({
            pathname: '/addMeasureToSensor',
            state: this.props.location.state,
        })
    }


    render() {
        return (

            <Container maxWidth="lg">
                <br></br>
                <div>
                    <button onClick={() => this.add()} className="btn btn-primary" style={{ marginLeft: "25px" }}> Add Measure</button>
                </div>
                <br></br>
                <Container maxWidth="lg">
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"> Data </TableCell>
                                    <TableCell align="center"> Valoare </TableCell>
                            
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.measures.map((measure) => (
                                    <TableRow
                                        key={measure.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center">{measure.dateTime}</TableCell>
                                        <TableCell align="center">{measure.value}</TableCell>
                                      

                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br></br>

                </Container>
            </Container>
        )
    }

}

export default ViewMeasure;