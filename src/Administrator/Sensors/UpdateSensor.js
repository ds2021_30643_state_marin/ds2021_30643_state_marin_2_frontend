import React from "react"
import { Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";


class UpdateSensor extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            description: "",
            maxValue:""

        }
    }

    componentDidMount() {
        axiosInstance.get("/senzor/" + this.state.id)
            .then(res => {
                let sensor = res.data;
                this.setState({
                    description: sensor.description,
                    maxValue: sensor.maxValue
                });
            })
            .catch(error => {
            })
    }


    handleInput = event => {
        const { value, name } = event.target;

        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let sensor = {
            id: this.state.id,
            description: this.state.description,
            maxValue: this.state.maxValue
        }

        console.log(sensor)
       
        axiosInstance.put("/senzor", sensor)
            .then(
                res => {
                    sensor = res.data;
                    this.props.history.push("/sensorsList")
                }
            )
            .catch(error => {
            });
    }

    render() {

        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {
            return (
                <React.Fragment>

                    <form onSubmit={this.handleSubmit}>
                        <Typography variant="h6" color="textPrimary" component="h6">
                            Update sensor
                        </Typography>
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Description:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.description}
                                    id="required"
                                    label="Description"
                                    name="description"
                                    placeholder="description"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Maximum value:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.maxValue}
                                    id="required"
                                    label="maxValue"
                                    name="maxValue"
                                    placeholder="maxValue"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>

                           
                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="primary"
                                id="create"
                                type="submit"
                            >
                                Update Sensor
                            </Button>
                        </div>
                    </form>
                </React.Fragment>



            )

        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }

}

export default UpdateSensor;