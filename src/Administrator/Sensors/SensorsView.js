import React from "react"
import axiosInstance from "../../axios";
import {
    Table, TableCell, TableBody, TableContainer,
    TableHead, TableRow, Paper
} from '@mui/material';
import Container from "@material-ui/core/Container";
class SensorsView extends React.Component {

    constructor() {
        super();
        this.state = {
            sensors: [],
        }

    }
    componentDidMount() {
        axiosInstance.get("/senzor")
            .then(res => {
                const val = res.data;
                this.setState({
                    sensors: val
                });
                console.log(val);
                console.log(this.state.sensors);
            }
            )
            .catch(error => {
                console.log(error);
            })
    }


    view = (id) => {
        this.props.history.push({
            pathname: '/sensor-measure',
            state: id
        })
    }

    updateSensor = (id) => {
        this.props.history.push({
            pathname: '/updateSensor',
            state: id
        })
    }

    deleteSensor = (sensorId) => {
        console.log(sensorId);
        axiosInstance.delete("/senzor/delete/" + sensorId)
        .then( () => {
            this.componentDidMount();
        })
        .catch( error => {
            console.log("Eroare la delete");
            console.log(error);
        })
    }
    
    addSensor(){
        this.props.history.push({
            pathname: '/addSensor',
        })
    }

    render() {
        return (

            <Container maxWidth="lg">
                <br></br>
                <div>
                    <button onClick={() => this.addSensor()} className="btn btn-primary"  style={{ marginLeft: "25px" }}> Add Sensor</button>
                </div>
                <br></br>
                <Container maxWidth="lg">
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"> Description </TableCell>
                                    <TableCell align="center"> Max Value </TableCell>
                                   
                                 
                                    <TableCell align="center"> Actions </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.sensors.map((sensor) => (
                                    <TableRow
                                        key={sensor.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center">{sensor.description}</TableCell>
                                        <TableCell align="center">{sensor.maxValue}</TableCell>
                                      
                                     
                                        <TableCell align="center">
                                            <button onClick={() => this.updateSensor(sensor.id)} className="btn btn-info"> Update </button>
                                            <button style={{ marginLeft: "10px" }} onClick={() => this.deleteSensor(sensor.id)} className="btn btn-danger">Delete </button>
                                            <button style={{ marginLeft: "10px" }} onClick={() => this.view(sensor.id)} className="btn btn-info">View measure </button>

                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br></br>

                </Container>
            </Container>
        )
    }



}

export default SensorsView;