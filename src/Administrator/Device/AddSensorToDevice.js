import React from "react"
import {  Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";

class AddSensorToDevice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            description: "",
            maxValue:""
           
        }
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let sensor = {
            deviceId: this.state.id,
            description: this.state.description,
            maxValue: this.state.maxValue
           
        }
        

        axiosInstance.put("/device/update", sensor)
            .then(
                res => {
                    sensor = res.data;
                }
            )
            .catch(error => {
            });
    }


    render() {
        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {

            return (
                <React.Fragment>
                    <form onSubmit={this.handleSubmit}>
                       
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Description:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Description"
                                    name="description"
                                    placeholder="Description"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Maximum value:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="maxValue"
                                    name="maxValue"
                                    placeholder="maxValue"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>
                           
                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="primary"
                                id="create"
                                type="submit"
                            >
                                Adauga senzor
                            </Button>
                        </div>
                    </form>
                 

                </React.Fragment>



            )
        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }
}

export default AddSensorToDevice;