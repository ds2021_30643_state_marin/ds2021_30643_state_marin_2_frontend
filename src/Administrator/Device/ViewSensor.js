import React from "react"
import axiosInstance from "../../axios";
import {
    Table, TableCell, TableBody, TableContainer,
    TableHead, TableRow, Paper
} from '@mui/material';
import Container from "@material-ui/core/Container";

class ViewSensor extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            sensor:"",

        }

    }

    componentDidMount() {
        axiosInstance.get("/device/senzor/" + this.state.id )
            .then(res => {
                const val = res.data;
                this.setState({
                    sensor: val
                });
                console.log(val);
                console.log(this.state.sensor);
            }
            )
            .catch(error => {
                console.log(error);
            })
    }

    addSensor(){
        this.props.history.push({
            pathname: '/addSensorToDevice',
            state: this.props.location.state,
        })
    }


    render() {
        return (

            <Container maxWidth="lg">
                <br></br>
                <div>
                    <button onClick={() => this.addSensor()} className="btn btn-primary"  style={{ marginLeft: "25px" }}> Add Sensor</button>
                </div>
                <br></br>
                <Container maxWidth="lg">
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center">  Description </TableCell>
                                    <TableCell align="center">  Max Value </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                    <TableRow
                                        key={this.state.sensor.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center">{this.state.sensor.description}</TableCell>
                                        <TableCell align="center">{this.state.sensor.maxValue}</TableCell>
                                       
                                        
                                    </TableRow>
                                
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br></br>

                </Container>
            </Container>
        )
    }



}

export default ViewSensor;