import React from "react"
import {  Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";

class AddDevice extends React.Component {
    constructor() {
        super();
        this.state = {
            description: "",
            address: "",
            maxEnergyCons: "",
            averageEnergyCons: ""
          
        }
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let device = {
            description: this.state.description,
            address: this.state.address,
            maxEnergyCons: this.state.maxEnergyCons,
            averageEnergyCons: this.state.averageEnergyCons
          
        }
    
        axiosInstance.post("/device", device)
            .then(
                res => {
                    device = res.data;
                    window.location.href = "/deviceList"
                }
            )
            .catch(error => {
            });
    }


    render() {
        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {

            return (
                <React.Fragment>
                    <form onSubmit={this.handleSubmit}>
                       
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Description:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Description"
                                    name="description"
                                    placeholder="Description"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Address:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Address"
                                    name="address"
                                    placeholder="Address"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    maxEnergyCons:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="maxEnergyCons"
                                    name="maxEnergyCons"
                                    placeholder="maxEnergyCons"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    averageEnergyCons:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    name="averageEnergyCons"
                                    placeholder="averageEnergyCons"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>

                           

                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="primary"
                                id="create"
                                type="submit"
                            >
                                Adauga device
                            </Button>
                        </div>
                    </form>
                    

                </React.Fragment>



            )
        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }
}

export default AddDevice;