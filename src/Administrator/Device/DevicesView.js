import React from "react"
import axiosInstance from "../../axios";
import {
    Table, TableCell, TableBody, TableContainer,
    TableHead, TableRow, Paper
} from '@mui/material';
import Container from "@material-ui/core/Container";
class DevicesView extends React.Component {

    constructor() {
        super();
        this.state = {
            devices: [],
        }

    }
    componentDidMount() {
        axiosInstance.get("/device")
            .then(res => {
                const val = res.data;
                this.setState({
                    devices: val
                });
                console.log(val);
                console.log(this.state.devices);
            }
            )
            .catch(error => {
                console.log(error);
            })
    }


    viewSensor = (id) => {
        this.props.history.push({
            pathname: '/device-sensor',
            state: id
        })
    }

    updateDevice = (id) => {
        this.props.history.push({
            pathname: '/updateDevice',
            state: id
        })
    }

    deleteDevice = (deviceId) => {
        console.log(deviceId);
        axiosInstance.delete("/device/delete/" + deviceId)
        .then( () => {
            this.componentDidMount();
        })
        .catch( error => {
            console.log("Eroare la delete");
            console.log(error);
        })
    }
    
    addDevice(){
        this.props.history.push({
            pathname: '/addDevice',
        })
    }

    render() {
        return (

            <Container maxWidth="lg">
                <br></br>
                <div>
                    <button onClick={() => this.addDevice()} className="btn btn-primary"  style={{ marginLeft: "25px" }}> Add Device</button>
                </div>
                <br></br>
                <Container maxWidth="lg">
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"> Description </TableCell>
                                    <TableCell align="center"> Address </TableCell>
                                    <TableCell align="center"> Max Energy Consumption </TableCell>
                                    <TableCell align="center"> Average Energy Consumption </TableCell>
                                 
                                    <TableCell align="center"> Actions </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.devices.map((device) => (
                                    <TableRow
                                        key={device.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center">{device.description}</TableCell>
                                        <TableCell align="center">{device.address}</TableCell>
                                        <TableCell align="center">{device.maxEnergyCons}</TableCell>
                                        <TableCell align="center">{device.averageEnergyCons}</TableCell>
                                     
                                        <TableCell align="center">
                                            <button onClick={() => this.updateDevice(device.id)} className="btn btn-info"> Update </button>
                                            <button style={{ marginLeft: "10px" }} onClick={() => this.deleteDevice(device.id)} className="btn btn-danger">Delete </button>
                                            <button style={{ marginLeft: "10px" }} onClick={() => this.viewSensor(device.id)} className="btn btn-info">View sensor </button>

                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br></br>

                </Container>
            </Container>
        )
    }



}

export default DevicesView;