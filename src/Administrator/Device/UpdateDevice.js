import React from "react"
import {  Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";


class UpdateDevice extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            description: "",
            address: "",
            maxEnergyCons: "",
            averageEnergyCons: ""

        }
    }

    componentDidMount() {
        axiosInstance.get("/device/" + this.state.id)
            .then(res => {
                let device = res.data;
                this.setState({
                    description: device.description,
                    address: device.address,
                    maxEnergyCons: device.maxEnergyCons,
                    averageEnergyCons: device.averageEnergyCons
                   
                });
            })
            .catch(error => {
            })
    }


    handleInput = event => {
        const { value, name } = event.target;

        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let device = {
            id: this.state.id,
            description: this.state.description,
            address: this.state.address,
            maxEnergyCons: this.state.maxEnergyCons,
            averageEnergyCons: this.state.averageEnergyCons        
        }

        console.log(device)
       
        axiosInstance.put("/device", device)
            .then(
                res => {
                    device = res.data;
                    this.props.history.push("/deviceList")
                }
            )
            .catch(error => {
            });
    }

    render() {

        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {
            return (
                <React.Fragment>

                    <form onSubmit={this.handleSubmit}>
                        <Typography variant="h6" color="textPrimary" component="h6">
                            Update device
                        </Typography>
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Description:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.description}
                                    id="required"
                                    label="Description"
                                    name="description"
                                    placeholder="description"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Address:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.address}
                                    id="required"
                                    label="Address"
                                    name="address"
                                    placeholder="Address"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Consum Maxim Energie:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.maxEnergyCons}
                                    id="required"
                                    label="maxEnergyCons"
                                    name="maxEnergyCons"
                                    placeholder="maxEnergyCons"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Consum mediu energie:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.averageEnergyCons}
                                    id="required"
                                    label="averageEnergyCons"
                                    name="averageEnergyCons"
                                    placeholder="averageEnergyCons"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>
                            
                           

                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="primary"
                                id="create"
                                type="submit"
                            >
                                Update Device
                            </Button>
                        </div>
                    </form>
                </React.Fragment>



            )

        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }

}

export default UpdateDevice;