import React from "react"



class Admin extends React.Component {
    
    render() {
        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {
            return (
                <React.Fragment>
                    <h1>
                        <a href="/clientsList" >
                            Clients List
                        </a>
                    </h1>
                    <h1>
                        <a href="/deviceList" >
                            Devices List
                        </a>
                    </h1>
                    <h1>
                        <a href="/sensorsList" >
                            Sensors List
                        </a>
                    </h1>
                </React.Fragment>
            )
        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }
}

export default Admin;