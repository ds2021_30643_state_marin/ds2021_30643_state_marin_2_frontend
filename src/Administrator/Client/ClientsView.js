import React from "react"
import axiosInstance from "../../axios";
import {
    Table, TableCell, TableBody, TableContainer,
    TableHead, TableRow, Paper
} from '@mui/material';
import Container from "@material-ui/core/Container";
class ClientsView extends React.Component {

    constructor() {
        super();
        this.state = {
            clients: [],

        }

    }

    componentDidMount() {
        axiosInstance.get("/client")
            .then(res => {
                const val = res.data;
                this.setState({
                    clients: val
                });
                console.log(val);
                console.log(this.state.clients);
            }
            )
            .catch(error => {
                console.log(error);
            })
    }


    viewDevices = (id) => {
        this.props.history.push({
            pathname: '/device-client',
            state: id
        })
    }

    updateClient = (id) => {
        this.props.history.push({
            pathname: '/updateClient',
            state: id
        })
    }

    deleteClient = (clientId) => {
        console.log(clientId);
        axiosInstance.delete("/client/delete/" + clientId)
        .then( () => {
            this.componentDidMount();
        })
        .catch( error => {
            console.log("Eroare la delete");
            console.log(error);
        })
    }
    
    addClient(){
        this.props.history.push({
            pathname: '/addClient',
        })
    }

    render() {
        return (

            <Container maxWidth="lg">
                <br></br>
                <div>
                    <button onClick={() => this.addClient()} className="btn btn-primary"  style={{ marginLeft: "25px" }}> Add Client</button>
                </div>
                <br></br>
                <Container maxWidth="lg">
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"> Client Name </TableCell>
                                    <TableCell align="center"> Client BirthDate </TableCell>
                                    <TableCell align="center"> Client Address </TableCell>
                                    <TableCell align="center"> Client Name </TableCell>
                                    <TableCell align="center"> Client Username </TableCell>
                                    <TableCell align="center"> Actions </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.clients.map((client) => (
                                    <TableRow
                                        key={client.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center">{client.name}</TableCell>
                                        <TableCell align="center">{client.birthdate}</TableCell>
                                        <TableCell align="center">{client.address}</TableCell>
                                        <TableCell align="center">{client.name}</TableCell>
                                        <TableCell align="center">{client.username}</TableCell>
                                        <TableCell align="center">
                                            <button onClick={() => this.updateClient(client.id)} className="btn btn-info"> Update </button>
                                            <button style={{ marginLeft: "10px" }} onClick={() => this.deleteClient(client.id)} className="btn btn-danger">Delete </button>
                                            <button style={{ marginLeft: "10px" }} onClick={() => this.viewDevices(client.id)} className="btn btn-info">View devices </button>

                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br></br>

                </Container>
            </Container>
        )
    }



}

export default ClientsView;