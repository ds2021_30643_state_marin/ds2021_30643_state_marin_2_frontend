import React from "react"
import {  Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";

class AddDeviceToClient extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            description: "",
            addresss: "",
            maxEnergyCons: "",
            averageEnergyCons: "",
           
        }
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let device = {
            clientId: this.state.id,
            description: this.state.description,
            address: this.state.address,
            maxEnergyCons: this.state.maxEnergyCons,
            averageEnergyCons: this.state.averageEnergyCons,
           
        }
        

        axiosInstance.put("/client/update", device)
            .then(
                res => {
                    device = res.data;
                    
                   // window.location.href = "/device-client"
                }
            )
            .catch(error => {
            });
    }


    render() {
        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {

            return (
                <React.Fragment>
                    <form onSubmit={this.handleSubmit}>
                       
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Description:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Description"
                                    name="description"
                                    placeholder="Description"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Address:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Address"
                                    name="addresss"
                                    placeholder="Address"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Maximum Energy Cosnumption:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="maxEnergyCons"
                                    name="maxEnergyCons"
                                    placeholder="maxEnergyCons"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "number"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Average Energy Consumption:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    name="averageEnergyCons"
                                    placeholder="averageEnergyCons"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    label="averageEnergyCons"
                                    type = "number"
                       
                                />
                            </div>

                           
                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="primary"
                                id="create"
                                type="submit"
                            >
                                Adauga device
                            </Button>
                        </div>
                    </form>
                    {this.state.invalid === true ? <h2>Format invalid!</h2> : null}

                </React.Fragment>



            )
        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }
}

export default AddDeviceToClient;