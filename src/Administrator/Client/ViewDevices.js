import React from "react"
import axiosInstance from "../../axios";
import {
    Table, TableCell, TableBody, TableContainer,
    TableHead, TableRow, Paper
} from '@mui/material';
import Container from "@material-ui/core/Container";

class ViewDevices extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            devices: [],

        }

    }

    componentDidMount() {
        axiosInstance.get("/client/devices/" + this.state.id )
            .then(res => {
                const val = res.data;
                this.setState({
                    devices: val
                });
                console.log(val);
                console.log(this.state.devices);
            }
            )
            .catch(error => {
                console.log(error);
            })
    }

    addDevice(){
        this.props.history.push({
            pathname: '/addDeviceToClient',
            state: this.props.location.state,
        })
    }


    render() {
        return (

            <Container maxWidth="lg">
                <br></br>
                <div>
                    <button onClick={() => this.addDevice()} className="btn btn-primary"  style={{ marginLeft: "25px" }}> Add Devices</button>
                </div>
                <br></br>
                <Container maxWidth="lg">
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"> Device Description </TableCell>
                                    <TableCell align="center"> Device Address </TableCell>
                                    <TableCell align="center"> Maximum energy conspumtion </TableCell>
                                    <TableCell align="center"> Average energy consumption </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.devices.map((device) => (
                                    <TableRow
                                        key={device.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center">{device.description}</TableCell>
                                        <TableCell align="center">{device.address}</TableCell>
                                        <TableCell align="center">{device.maxEnergyCons}</TableCell>
                                        <TableCell align="center">{device.averageEnergyCons}</TableCell>
                                        
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br></br>

                </Container>
            </Container>
        )
    }



}

export default ViewDevices;