import React from "react"
import { Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";





class UpdateClient extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            name: "",
            username: "",
            password: "",
            address: "",
            birthdate: ""

        }
    }

    componentDidMount() {
        axiosInstance.get("/client/" + this.state.id)
            .then(res => {
                let client = res.data;
                this.setState({
                    name: client.name,
                    username: client.username,
                    password: client.password,
                    address: client.address,
                    birthdate: client.birthdate,
                });
            })
            .catch(error => {
            })
    }


    handleInput = event => {
        const { value, name } = event.target;

        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let client = {
            id: this.state.id,
            birthdate: this.state.birthdate,
            address: this.state.address,
            name: this.state.name,
            username: this.state.username,
            password: this.state.password,
        
        }
       
        axiosInstance.put("/client", client)
            .then(
                res => {
                    client = res.data;
                    this.props.history.push("/clientsList")
                }
            )
            .catch(error => {
            });
    }

    render() {

        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {
            return (
                <React.Fragment>

                    <form onSubmit={this.handleSubmit}>
                        <Typography variant="h6" color="textPrimary" component="h6">
                            Update client
                        </Typography>
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Name:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.name}
                                    id="required"
                                    label="Name"
                                    name="name"
                                    placeholder="Name"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Username:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.username}
                                    id="required"
                                    label="Username"
                                    name="username"
                                    placeholder="Username"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Password:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.password}
                                    id="required"
                                    label="Password"
                                    name="password"
                                    placeholder="Password"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "password"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Birthdate:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.birthdate}
                                    id="required"
                                    //label="Birthdate"
                                    name="birthdate"
                                    placeholder="Birthdate"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "date"
                                />
                            </div>
                            
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Address:
                                </Typography>
                                <TextField
                                    required={true}
                                    value={this.state.address}
                                    id="required"
                                    label="Address"
                                    name="address"
                                    placeholder="Address"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                   // type = "date"
                                />
                            </div>

                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="primary"
                                id="create"
                                type="submit"
                            >
                                Update Client
                            </Button>
                        </div>
                    </form>
                </React.Fragment>



            )

        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }

}

export default UpdateClient;