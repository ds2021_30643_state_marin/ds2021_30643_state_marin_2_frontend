import React from "react"
import { Button, Typography, TextField } from '@material-ui/core';
import axiosInstance from "../../axios";

class AddClient extends React.Component {
    constructor() {
        super();
        this.state = {
            name: "",
            username: "",
            password: "",
            address: "",
            birthdate: ""
        }
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        })
    }

    handleSubmit = event => {
        event.preventDefault();
        let client = {
            name: this.state.name,
            username: this.state.username,
            password: this.state.password,
            address: this.state.address,
            birthdate: this.state.birthdate,
        }
        this.props.history.push("/clientsList")

        axiosInstance.post("/client", client)
            .then(
                res => {
                    client = res.data;
                    window.location.href = "/clientsList"
                }
            )
            .catch(error => {
            });
    }


    render() {
        if (localStorage.getItem("USER_ROLE") === "ADMINISTRATOR") {

            return (
                <React.Fragment>
                    <form onSubmit={this.handleSubmit}>
                       
                        <div>
                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Name:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Name"
                                    name="name"
                                    placeholder="Name"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Username:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Username"
                                    name="username"
                                    placeholder="Username"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Password:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Password"
                                    name="password"
                                    placeholder="password"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "password"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Birthdate:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    name="birthdate"
                                    placeholder="Birthdate"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                    type = "date"
                                />
                            </div>

                            <div>
                                <Typography
                                    variant="subtitle2"
                                    color="textPrimary"
                                    component="h2"
                                >
                                    Address:
                                </Typography>
                                <TextField
                                    required={true}

                                    id="required"
                                    label="Address"
                                    name="address"
                                    placeholder="Address"
                                    onChange={this.handleInput}
                                    margin="normal"
                                    variant="outlined"
                                    autoComplete="off"
                                />
                            </div>

                        </div>
                        <div id="buttons">
                            <Button
                                variant="contained"
                                color="secondary"
                                id="create"
                                type="submit"
                            >
                                Adauga client
                            </Button>
                        </div>
                    </form>
                    {this.state.invalid === true ? <h2>Format invalid!</h2> : null}

                </React.Fragment>



            )
        }
        else {
            throw new Error("Doar administratorul are acces");
        }
    }
}

export default AddClient;