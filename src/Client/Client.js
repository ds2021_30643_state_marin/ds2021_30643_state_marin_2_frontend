import React from "react"



class Client extends React.Component {

    
    render() {

        if (localStorage.getItem("USER_ROLE") === "CLIENT") {

            return (

                <React.Fragment>
                    <h1>
                        <a href="/myDevices" >
                            My Devices
                        </a>
                    </h1>
                    <h1>
                        <a href="/mySensor" >
                            My Sensor
                        </a>
                    </h1>
                    <h1>
                        <a href="/myMeasurement" >
                            My measurement
                        </a>
                    </h1>
                    <h1>
                        <a href="/barChart" >
                            My graph
                        </a>
                    </h1>
                </React.Fragment>

            )
        }
        else {
            throw new Error("Doar clientul are acces");
        }

    }
}

export default Client;