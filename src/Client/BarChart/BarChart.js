import React from "react";
import axiosInstance from "../../axios";
import TextField from "@material-ui/core/TextField";
import { Bar } from 'react-chartjs-2';
import Button from '@mui/material/Button';
import { Grid } from "@material-ui/core";
import Container from "@material-ui/core/Container";


class BarChart extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            energyList: [],
            date: "",
        }
    }


    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
        console.log(value);
    };

    onSubmitFun = event => {

        event.preventDefault();
        let client = {
            clientId: localStorage.getItem("USER_ID"),
            date: this.state.date
        }

        axiosInstance.post("/client/barChart", client)
            .then(
                res => {
                    const val = res.data;
                    this.setState({
                        energyList: val
                    });
                    console.log("Succes");
                    console.log(this.state.energyList);

                }
            )
            .catch(error => {

            })
    }

    render() {
        if (localStorage.getItem("USER_ROLE") === "CLIENT") {
            return (
                <Container maxWidth="sm">
                    <div>
                        <Grid>
                            <form onSubmit={this.onSubmitFun}>
                                <TextField
                                    variant="outlined"
                                    margin="normal"
                                    required
                                    fullWidth
                                    id="date"
                                    label="Date"
                                    name="date"
                                    onChange={this.handleInput}
                                    autoFocus
                                    type="date"
                                />

                                <Button
                                    type="submit"
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                >
                                    SUBMIT
                                </Button>
                            </form>
                        </Grid>
                    </div>
                    <Container>
                    <div>
                  <Bar
                    data={{
                        labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14",
                            "15", "16", "17", "18", "19", "20", "21", "22", "23"],
                        datasets: [
                            {
                                label: 'Client historical energy consumption',
                                backgroundColor: 'rgba(75,192)',
                                borderColor: 'rgba(0,1)',
                                borderWidth: 2,
                                data: [
                                    this.state.energyList[0],
                                    this.state.energyList[1],
                                    this.state.energyList[2],
                                    this.state.energyList[3],
                                    this.state.energyList[4],
                                    this.state.energyList[5],
                                    this.state.energyList[6],
                                    this.state.energyList[7],
                                    this.state.energyList[8],
                                    this.state.energyList[9],
                                    this.state.energyList[10],
                                    this.state.energyList[11],
                                    this.state.energyList[12],
                                    this.state.energyList[13],
                                    this.state.energyList[14],
                                    this.state.energyList[15],
                                    this.state.energyList[16],
                                    this.state.energyList[17],
                                    this.state.energyList[18],
                                    this.state.energyList[19],
                                    this.state.energyList[20],
                                    this.state.energyList[21],
                                    this.state.energyList[22],
                                    this.state.energyList[23],
                                ]
                            }
                        ]
                    }
                    }
                    options={{
                        title: {
                            display: true,
                            text: 'View historical energy consumption for each client',
                            fontSize: 10
                        },
                        legend: {
                            display: true,
                            position: 'center'
                        }
                    }}
                />
            </div>

                    </Container>
                </Container>
                







            );
        }
        else {
            return <div>
                You need to login as a client
            </div>;
        }
    }



}



//     if (data_role === "CLIENT") {
//         return (

//             <div>
//                 <Bar
//                     data={{
//                         labels: ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14",
//                             "15", "16", "17", "18", "19", "20", "21", "22", "23"],
//                         datasets: [
//                             {
//                                 label: 'Client historical energy consumption',
//                                 backgroundColor: 'rgba(75,192)',
//                                 borderColor: 'rgba(0,1)',
//                                 borderWidth: 2,
//                                 data: [
//                                     this.state.energyList[0],
//                                     this.state.energyList[1],
//                                     this.state.energyList[2],
//                                     this.state.energyList[3],
//                                     this.state.energyList[4],
//                                     this.state.energyList[5],
//                                     this.state.energyList[6],
//                                     this.state.energyList[7],
//                                     this.state.energyList[8],
//                                     this.state.energyList[9],
//                                     this.state.energyList[10],
//                                     this.state.energyList[11],
//                                     this.state.energyList[12],
//                                     this.state.energyList[13],
//                                     this.state.energyList[14],
//                                     this.state.energyList[15],
//                                     this.state.energyList[16],
//                                     this.state.energyList[17],
//                                     this.state.energyList[18],
//                                     this.state.energyList[19],
//                                     this.state.energyList[20],
//                                     this.state.energyList[21],
//                                     this.state.energyList[22],
//                                     this.state.energyList[23],
//                                 ]
//                             }
//                         ]
//                     }
//                     }
//                     options={{
//                         title: {
//                             display: true,
//                             text: 'View historical energy consumption for each client',
//                             fontSize: 10
//                         },
//                         legend: {
//                             display: true,
//                             position: 'center'
//                         }
//                     }}
//                 />
//             </div>

//         );
//     }

//     else {
//         return <div>
//             You need to login as a client
//         </div>;
//     }
// }





export default BarChart;