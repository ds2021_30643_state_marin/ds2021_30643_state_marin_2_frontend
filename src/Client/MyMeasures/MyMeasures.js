import React from "react"
import axiosInstance from "../../axios";
import {
    Table, TableCell, TableBody, TableContainer,
    TableHead, TableRow, Paper
} from '@mui/material';
import Container from "@material-ui/core/Container";

import * as SockJS from "sockjs-client";
import * as Stomp from "stompjs";

class MyMeasures extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            id: this.props.location.state,
            measures: [],

        }

    }

    connect() {
       // const URL = "http://localhost:8080/socket";
       const URL ="https://spring-state-marin-2.herokuapp.com/socket";
        const websocket = new SockJS(URL);
        const stompClient = Stomp.over(websocket);
        stompClient.connect({}, frame => {
            console.log("CONNECTED TO: " + frame);
            
            stompClient.subscribe("/topic/socket/measure/msg", notification => {
                let message = notification.body;
                console.log("MESAJ DE NOTIFICARE = " + message);
                alert(message);
            })
            stompClient.subscribe("/topic/socket/measure/measuree", notification => {
                const e = JSON.parse(notification.body);
                console.log(e);
                const prev = this.state.measures;
                prev.push(e);
                this.setState({measures: prev});
                console.log(this.state.measures);
             
            })
           
        })
    }

    componentDidMount() {
        this.connect();
        axiosInstance.get("/client/myMeasure/" + localStorage.getItem("USER_ID"))
            .then(res => {
                const val = res.data;
                this.setState({
                    measures: val
                });
                console.log(val);
                console.log(this.state.measures);
            }
            )
            .catch(error => {
                console.log(error);
            })
    }



    render() {
        return (

            <Container maxWidth="lg">
               
                <Container maxWidth="lg">
                    <TableContainer component={Paper}>
                        <Table aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell align="center"> Data </TableCell>
                                    <TableCell align="center"> Valoare </TableCell>
                            
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {this.state.measures.map((measure) => (
                                    <TableRow
                                        key={measure.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell align="center">{measure.dateTime}</TableCell>
                                        <TableCell align="center">{measure.value}</TableCell>
                                      

                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br></br>

                </Container>
            </Container>
        )
    }

}

export default MyMeasures;