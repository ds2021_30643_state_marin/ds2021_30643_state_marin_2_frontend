import './App.css';
import '../src/Components/Navbar'
import {
  BrowserRouter as Router,
  Route,
  Switch,

} from "react-router-dom";
import NavBar from './Components/Navbar';
import LogIn from '../src/Authentication/LogIn';
import Admin from './Administrator/Admin';
import Client from './Client/Client';
import ClientsView from './Administrator/Client/ClientsView';
import UpdateClient from './Administrator/Client/UpdateClient';
import AddClient from './Administrator/Client/AddClient';
import ViewDevices from './Administrator/Client/ViewDevices';
import AddDeviceToClient from './Administrator/Client/AddDeviceToClient';
import DevicesView from './Administrator/Device/DevicesView';
import AddDevice from './Administrator/Device/AddDevice';
import UpdateDevice from './Administrator/Device/UpdateDevice';
import ViewSensor from './Administrator/Device/ViewSensor';
import AddSensorToDevice from './Administrator/Device/AddSensorToDevice';
import SensorsView from './Administrator/Sensors/SensorsView';
import AddSensor from './Administrator/Sensors/AddSensor';
import UpdateSensor from './Administrator/Sensors/UpdateSensor';
import ViewMeasure from './Administrator/Sensors/ViewMeasure';
import AddMeasureToSensor from './Administrator/Sensors/AddMeasureToSensor';
import MyDevices from './Client/MyDevices/MyDevices';
import MySensors from './Client/MySensors/MySensors';
import MyMeasures from './Client/MyMeasures/MyMeasures';
import BarChart from './Client/BarChart/BarChart';

function App() {
  return (
    <Router>
      <NavBar/>
      <Switch>
      <Route exact path = "/log-in" exact component={LogIn}/>
      <Route exact path = "/administrator" exact component={Admin}/>
      <Route exact path = "/client" exact component={Client}/>
      <Route exact path = "/clientsList" exact component={ClientsView}/>
      <Route exact path = "/updateClient" exact component={UpdateClient}/>
      <Route exact path = "/addClient" exact component={AddClient}/>
      <Route exact path = "/device-client" exact component={ViewDevices}/>
      <Route exact path = "/addDeviceToClient" exact component={AddDeviceToClient}/>
      <Route exact path = "/deviceList" exact component={DevicesView}/>
      <Route exact path = "/addDevice" exact component={AddDevice}/>
      <Route exact path = "/updateDevice" exact component={UpdateDevice}/>
      <Route exact path = "/device-sensor" exact component={ViewSensor}/>
      <Route exact path = "/addSensorToDevice" exact component={AddSensorToDevice}/>
      <Route exact path = "/sensorsList" exact component={SensorsView}/>
      <Route exact path = "/addSensor" exact component={AddSensor}/>
      <Route exact path = "/updateSensor" exact component={UpdateSensor}/>
      <Route exact path = "/sensor-measure" exact component={ViewMeasure}/>
      <Route exact path = "/addMeasureToSensor" exact component={AddMeasureToSensor}/>
      <Route exact path = "/myDevices" exact component={MyDevices}/>
      <Route exact path = "/mySensor" exact component={MySensors}/>
      <Route exact path = "/myMeasurement" exact component={MyMeasures}/>
      <Route exact path = "/barChart" exact component={BarChart}/>
      

      
      
      

     


      </Switch>

    </Router>
  );
}

export default App;
