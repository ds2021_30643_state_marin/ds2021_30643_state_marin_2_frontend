import React from "react";
import {
  AppBar,
  Toolbar,
  CssBaseline,
  Typography,
  makeStyles,
} from "@material-ui/core";
import { Link } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  navlinks: {
    marginLeft: theme.spacing(10),
    display: "flex",
  },
 
  link: {
    textDecoration: "none",
    color: "white",
    fontSize: "20px",
    marginLeft: theme.spacing(2),
    marginRight: theme.spacing(5),
    "&:hover": {
      color: "black",
      
    },
  },
}));

function logOut(){
  localStorage.removeItem("USER_ID");
  localStorage.removeItem("USER_ROLE")

}

function Navbar() {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <CssBaseline />
      <Toolbar>
        <Typography variant="h4" className={classes.logo}></Typography>
          <div className={classes.navlinks}>
            {localStorage.getItem("USER_ID") === null ? <Link to="/log-in" className={classes.link}>
              Log In
            </Link> :  <Link to="/log-in" className={classes.link} onClick= {logOut}>
              Log Out
            </Link> }
            

          </div>
      </Toolbar>
    </AppBar>
  );
}
export default Navbar;
