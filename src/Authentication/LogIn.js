import React from "react";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Container from "@material-ui/core/Container";
import axiosInstance from "../axios";
import { Grid } from "@material-ui/core";

import 'react-toastify/dist/ReactToastify.css'

class Login extends React.Component {
    constructor() {
        super();
        this.state = {
            username: "",
            password: "",
            inexistent: false,
            loginSucces: {
                rol: "",
                id: 0
            }
        };
    }

    handleInput = event => {
        const { value, name } = event.target;
        this.setState({
            [name]: value
        });
        console.log(value);
    };

    onSubmitFun = event => {

        event.preventDefault();
        let credentilas = {
            username: this.state.username,
            password: this.state.password
        }

        axiosInstance.post("/login", credentilas)
            .then(
                res => {
                    const val = res.data;
                    this.setState({
                        loginSucces: val
                    });
                    console.log("Succes");
                    console.log(this.state.loginSucces);

                    localStorage.setItem("USER_ROLE", this.state.loginSucces.rol);
                    localStorage.setItem("USER_ID", this.state.loginSucces.id);



                    if (this.state.loginSucces.rol === "CLIENT") {
                        this.props.history.push("/client");
                    }
                    if (this.state.loginSucces.rol === "ADMINISTRATOR") {
                        this.props.history.push("/administrator");
                    }
                }
            )
            .catch(error => {
                this.setState({
                    inexistent: true,
                });
                console.log(this.state.inexistent);

            })
    }


    render() {

        return (

            <Container maxWidth="sm">
                <div>
                    <Grid>
                        <form onSubmit={this.onSubmitFun}>
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                id="username"
                                label="Username"
                                name="username"
                                autoComplete="string"
                                onChange={this.handleInput}
                                autoFocus
                            />
                            <TextField
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                type="password"
                                id="password"
                                onChange={this.handleInput}
                                autoComplete="current-password"
                            />
                            <Button
                                type="submit"
                                fullWidth
                                variant="contained"
                                color="primary"
                            >
                                Sign In
                            </Button>
                        </form>
                        {this.state.inexistent === true ? <span>Username or Password incorrect</span> : null}
                    </Grid>
                   
                    

                </div>
               
            </Container>
           
        );
    }

}

export default Login;

